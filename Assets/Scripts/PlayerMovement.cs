﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    public float deplacementForce = 500f;

    // Update is called once per frame
    // FixedUpdate is use when your messing with physics
    void FixedUpdate()
    {
        // adding a force on the z-axis
        if(Input.GetKey("w")){
            rb.AddForce(0, 0, deplacementForce * Time.deltaTime);
        }

        if(Input.GetKey("s")){
            rb.AddForce(0, 0, -deplacementForce * Time.deltaTime);
        }

        if(Input.GetKey("d")){
            rb.AddForce(deplacementForce * Time.deltaTime, 0, 0);
        }

        if(Input.GetKey("a")){
            rb.AddForce(-deplacementForce * Time.deltaTime, 0, 0);
        }
    }
}
