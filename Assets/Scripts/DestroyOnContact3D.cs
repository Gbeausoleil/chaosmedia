﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnContact3D : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }
}
